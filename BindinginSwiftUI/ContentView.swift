//
//  ContentView.swift
//  BindinginSwiftUI
//
//  Created by Artyom Romanchuk on 04.01.2021.
//

import SwiftUI

struct ContentView: View {
    @State private var isOn: Bool = false
    @State private var isPlaying: Bool = false
    @State private var placeHolderText = "Nothing"
    
    var body: some View {
        VStack{
            Text(isOn ? "On 🤓" : "Off 😎")
                .font(.custom("Arial", size: 100))
            SwithcView(isOn: $isOn)
            
            Text(isPlaying ? "Play " : "Pause")
                .font(.largeTitle)
            PlayButton(isPlaying: $isPlaying)
            
            VStack{
                Text("\(placeHolderText)")
                CustomButton1(placeHolderText: $placeHolderText)
                CustomButton2(placeHolderText: $placeHolderText)
            }.padding(50)
            
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct CustomButton1: View {
    @Binding var placeHolderText: String
    var body: some View{
        Button("Man Says") { placeHolderText = "Hello"}
    }
}

struct CustomButton2: View {
    @Binding var placeHolderText: String
    var body: some View{
        Button("Man2 Says") { placeHolderText = "Hi"}
    }
}

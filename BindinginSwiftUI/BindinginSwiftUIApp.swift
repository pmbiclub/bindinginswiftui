//
//  BindinginSwiftUIApp.swift
//  BindinginSwiftUI
//
//  Created by Artyom Romanchuk on 04.01.2021.
//

import SwiftUI

@main
struct BindinginSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  SwithcView.swift
//  BindinginSwiftUI
//
//  Created by Artyom Romanchuk on 04.01.2021.
//

import SwiftUI

struct SwithcView: View {
    @Binding var isOn: Bool
    
    var body: some View {
        Toggle(isOn: $isOn) {
            Text("")
        }.fixedSize()
    }
}

struct SwithcView_Previews: PreviewProvider {
    static var previews: some View {
        SwithcView(isOn: .constant(false))
    }
}
